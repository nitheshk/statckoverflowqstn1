##Salesforce: Creating a User Registration Form using VisualFocre


###https://stackoverflow.com/questions/45014924/salesforce-creating-a-user-registration-form-using-visualfocre/45036934#45036934

##Visual Page

```
#!HTML

<apex:page Controller="VFFileUpload">
<apex:pageMessages id="showmsg"></apex:pageMessages>
<apex:form>
<apex:pageBlock title="Upload Attachment">

<apex:pageBlockButtons location="top">
    <apex:commandButton value="Save" action="{!saveForm}"  />
    <apex:commandButton value="reset" action="{!resetForm}"  />
</apex:pageBlockButtons>

    <apex:pageBlockSection>
        <apex:inputField value="{!Registration_Forum.Name}" />
        <apex:inputField value="{!Registration_Forum.age__c}" />
        <apex:inputField value="{!Registration_Forum.Certification__c}" />
        <apex:inputField value="{!Registration_Forum.Project_Unit__c}" />
        <apex:selectRadio value="{!Registration_Forum.Gender__c}">
            <apex:selectOption itemValue="Male" itemLabel="Male" />
            <apex:selectOption itemValue="Female" itemLabel="Female" />
        </apex:selectRadio>
      <apex:inputFile id="file" value="{!fileBody}" filename="{!fileName}" /> 
    </apex:pageBlockSection>

</apex:pageBlock>
</apex:form>
</apex:page>




```


## Controller
```
#!Java


public class VFFileUpload
{
public Registration_Forum__c Registration_Forum{get;set;}
public String fileName {get;set;}
public Blob fileBody {get;set;}

public VFFileUpload()  {
    Registration_Forum=new Registration_Forum__c();
}

public void saveForm(){
     upsert Registration_Forum;
     if(fileBody != null && fileName != null && Registration_Forum.id!=null)
    {
      Attachment myAttachment  = new Attachment();
      myAttachment.Body = fileBody;
      myAttachment.Name = fileName;
      myAttachment.ParentId = Registration_Forum.Id;
      upsert myAttachment;
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.info,'File Upload  Success'));
    }
     ApexPages.addmessage(new ApexPages.message(ApexPages.severity.info,'Form Submission Success'));
}

public void  resetForm(){
    Registration_Forum=new Registration_Forum__c();
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.info,'Reset'));
}

}


```