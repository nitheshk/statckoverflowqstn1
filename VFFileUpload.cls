public class VFFileUpload
{
    public Registration_Forum__c Registration_Forum{get;set;}
    public String fileName {get;set;}
    public Blob fileBody {get;set;}
  
    public VFFileUpload()  {
        Registration_Forum=new Registration_Forum__c();
    }
    
    public void saveForm(){
         upsert Registration_Forum;
         if(fileBody != null && fileName != null && Registration_Forum.id!=null)
        {
          Attachment myAttachment  = new Attachment();
          myAttachment.Body = fileBody;
          myAttachment.Name = fileName;
          myAttachment.ParentId = Registration_Forum.Id;
          upsert myAttachment;
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.info,'File Upload  Success'));
        }
         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.info,'Form Submission Success'));
    }

    public void  resetForm(){
        Registration_Forum=new Registration_Forum__c();
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.info,'Reset'));
    }
   
}